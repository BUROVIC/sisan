const mongodb = require('mongodb').MongoClient;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config');

let wordsDB = null;
let users;

mongodb.connect(config.dbUri)
.then(db => {
    const database = db.db(config.dbName);
    console.log('Succesfull connection to Mongo');
    return database;
})
.then(database => {
    wordsDB = database;
    users = wordsDB.collection('users');
})
.catch(console.error);

function createUser(req, res, next) {
    if (wordsDB) {
        let newUser = req.body;
        let salt = bcrypt.genSaltSync(10);
        newUser.passwordHash = bcrypt.hashSync(newUser.password, salt);
        delete newUser.password;
        users.findOne({email: newUser.email}).then(userFromDB => {
            if (userFromDB === null) {
                return users.insertOne(newUser)
            } else {
                throw new Error("This user has already registered");
            }
        }).then(user => {
            console.log(user.ops);
            return user.ops;
        }).then(user => {
            res.json(user);
        }).catch(err => {
            console.error(err);
            res.json({error: err.message});
        })
        // user.compare = bcrypt.compareSync(user.password, user.passwordHash);    
    }
}

function login(req, res, next) {
    let loginUser = req.body;
    users.findOne({ email: loginUser.email }).then(userFromDB => {
        if (userFromDB) {

            loginUser.password = loginUser.password || '';
            let compare = bcrypt.compareSync(loginUser.password, userFromDB.passwordHash);
            console.log(userFromDB);
            if (!compare) throw new Error("Wrong password");
            return (compare) ? userFromDB : null;
        } else {
            throw new Error("Please sign up");
        }
    })
    .then(userFromDB => {
        let token = jwt.sign({ id: userFromDB._id }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });

        res.json({token});
    })
    .catch(err => {
        console.error(err);
        res.json({error: err.message});
    })
}

function allUsers(req, res, next) {
    users.find({}).toArray().then(allUsersFromDB => {
        res.json(allUsersFromDB);
    })
}

module.exports = {
    createUser,
    login,
    allUsers
}