const mongodb = require('mongodb').MongoClient;
const config = require('../config');

let wordsDB = null;
let users;
let answersCollection;

mongodb.connect(config.dbUri)
.then(db => {
    const database = db.db(config.dbName);
    console.log('Succesfull connection to Mongo');
    return database;
})
.then(database => {
    wordsDB = database;
    users = wordsDB.collection('users');
    answersCollection = wordsDB.collection('answers');
})
.catch(console.error);

async function userWinRate(userId){
    let [allAnswers, rightAnswers] = await Promise.all([answersCollection.find({ userID: userId }).toArray(), 
        answersCollection.find({userID: userId, isCorrect: true}).toArray()]);
        
    return rightAnswers.length / allAnswers.length;
}

async function currentUserWinRate(req, res, next){
    let bearerToken = req.headers['authorization'];
    let token = bearerToken.split(' ')[1];

    let winrate = await userWinRate(req.userId);
    res.json({"winrate" : winrate});
    
}

module.exports = {
    currentUserWinRate
}