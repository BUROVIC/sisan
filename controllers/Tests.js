const mongodb = require('mongodb').MongoClient;
const config = require('../config');
const readline = require('readline');
const fs = require('fs');

let wordsDB = null;
let wordsCollection;
let answersCollection;

mongodb.connect(config.dbUri)
.then(db => {
    const database = db.db(config.dbName);
    console.log('Succesfull connection to Mongo');
    return database;
})
.then(database => {
    wordsDB = database;
    wordsCollection = wordsDB.collection('words');
    answersCollection = wordsDB.collection('answers');

    var fs = require('fs'),
    readline = require('readline');

    // fillWordsDictionary('C:\\Users\\BUROVIC\\Source\\Repos\\3kwords\\3kWords\\Resources\\EnglishWords.txt',
    //     'C:\\Users\\BUROVIC\\Source\\Repos\\3kwords\\3kWords\\Resources\\RussianTranslate.txt');

    translates = {};
    
    wordsCollection.stats().then(stats =>{
        for (let i = 0; i < stats.count; i++) {
            wordsCollection.find().limit(-1).skip(i).next().then(word =>{
                translates[word.word] = word.translation;
            });
        }
    })
    if (!translates.length) {
        translates = { "abandon":  "отказаться от", "ability": "способность", "able": "в состоянии",
        "abortion" : "аборт",
        "about":"около",
        "above":"выше",
        "abroad":"за границу",
        "absence":"отсутствие",
        "absolute":"абсолютный",
        "absolutely":"абсолютно",
        "absorb":"абсорбировать",
        "abuse":"злоупотребление",
        "academic":"академический"
        }
    }
})
.catch(console.error);


var rightAnswers = {};

//need do get this from database
var translates = {};

function getWordByTranslation(translation) {
    return Object.keys(translates).find(key => translates[key] === translation);
  }

function randomWord(){
    return Object.keys(translates)[Math.floor(Math.random()*Object.keys(translates).length)];
}

function verify(req, res, next){
    let bearerToken = req.headers['authorization'];
    let token = bearerToken.split(' ')[1];

    var originalWord = randomWord();

    var rightAnswer;
    var isRight;
    if (token in rightAnswers){
        rightAnswer = rightAnswers[token];
        isRight = rightAnswer == req.body.answer;

        //Add to answers collection
        answersCollection.insertOne({userID: req.userId, date: new Date(), 
            word : getWordByTranslation(rightAnswer), isCorrect: isRight});
    }
    rightAnswers[token] = translates[originalWord];

    var suggestedTranslations = [];
    while(suggestedTranslations.length < 3){
        rndWord = randomWord();
        if(!suggestedTranslations.includes(translates[rndWord]) && rndWord != originalWord){
            suggestedTranslations.push(translates[rndWord]);
        }
    }
    suggestedTranslations.splice(Math.floor(Math.random() * 4), 0, translates[originalWord]);

    res.json({"answerState": {"isRight": isRight, "rightAnswer": rightAnswer}, "originalWord" : originalWord, "suggestedTranslations" : suggestedTranslations});
}

async function fillWordsDictionary(pathEngWords, pathRusTruns){
    //Clear collection
    wordsCollection.remove({});

    var words = [];
    var trans = [];

    readline.createInterface({
        input: fs.createReadStream(pathEngWords),
        output: process.stdout,
        console: false
    }).on('line', function(line) {
        words.push(line);
    }).on('close', function() {
        readline.createInterface({
            input: fs.createReadStream(pathRusTruns),
            output: process.stdout,
            console: false
        }).on('line', function(line) {
            trans.push(line);
        }).on('close', function() {
            var i = 0;
            console.log('length', words.length);
            for (let i = 0; i < words.length; i++) {
                var newWord = {word : words[i], translation : trans[i]};
                //translates[words[i]] = trans[i];
                wordsCollection.insertOne(newWord);
            }
        })
    })
}

module.exports = {
    verify
}