//Текущее словово
var engWord;
//Варианты перевода (здесь должны быть кнопки)
var buttonsTranslation = [];

function getNextWord(userAnswer){
    fetch('/api/verifyAnswer', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'authorization': 'Bearer ' + localStorage.getItem('token')
        },
        body: JSON.stringify({ 
            answer: userAnswer
        })
    })
    .then(data => data.json())
    .then(json => {
        console.log(json);
        engWord.innerHTML = json.originalWord;
        for (let i = 0; i < json.suggestedTranslations.length; i++) {
            buttonsTranslation[i].innerText = json.suggestedTranslations[i];
        }

        if (Object.keys(json.answerState).length) {
            const formCorrect = document.querySelector("#FormCorrect");
            const formIncorrect = document.querySelector("#FormIncorrect");
            const rightAnswer = document.querySelector("#rightAnswer");
    
            formCorrect.hidden = !json.answerState.isRight;
            formIncorrect.hidden = json.answerState.isRight;

            rightAnswer.innerHTML = json.answerState.rightAnswer;
        }

        calculateAndFillUserStatistics();
    })
}

function calculateAndFillUserStatistics(){
    fetch('/api/winrate', {
        method: 'GET',
        headers: {
            'authorization': 'Bearer ' + localStorage.getItem('token')
        }
    })
    .then(data => data.json())
    .then(json => {
        console.log(json);
    })
}

function allUsers() {
    fetch('/api/allusers', {
        method: 'GET',
        headers: {
            'authorization': 'Bearer ' + localStorage.getItem('token')
        }
    })
    .then(data => data.json())
    .then(json => {
        console.log(json);
    })
}

window.onload = function() {
    const enterButton = document.querySelector("#Enter");
    const samples = document.querySelector(".samples");
    const loginForm = document.forms.login;

    buttonsTranslation = document.querySelectorAll(".buttonTranslation");
    buttonsTranslation.forEach(b =>{
        b.addEventListener("click", function() {
            getNextWord(this.innerText);
        }, false);
    })
    engWord = document.querySelector("#theWord");

    enterButton.onclick = event => {
        //login or signup
        var submitType = document.querySelector('input[name="action"]:checked').value;

        const emailVal = document.querySelector("#email").value;
        const passwordVal = document.querySelector("#pass").value;
        fetch('/api/' + submitType, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ 
                email: emailVal,
                password: passwordVal
            })
        })
        .then(data => data.json())
        .then(json => {
            console.log(json);
            if (json.token){
                localStorage.setItem('token', json.token);
                document.getElementById('overlay').style.display='none';
                getNextWord();
            }
            else {
                //Need to change in markup
                var loginError;
                loginError = json.error ? json.error : (json.invalidProps != undefined ? "Invalid " + json.invalidProps.join(", ") : "");
                console.log(loginError);
            }
        });
    }

    //getNextWord();
}

function addSample() {
    console.log("addSample");
}