const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const nodemon = require('nodemon');
const path = require('path');

const serverPort = 4000;
const proxyPort = 3000;
const publicDir = path.join(__dirname, 'public')

gulp.task('nodemon', (done) => {
    nodemon('--inspect --ignore public/ --ignore node_modules/ --ignore gulpfile.js server.js --port ' + serverPort);
    nodemon.on('restart',  (files) => {
      browserSync.reload();
    });
    done();
});

gulp.task('browser-sync-init', (done) => {
    browserSync.init({
        proxy: `http://localhost:${serverPort}`,
        proxyPort: proxyPort
    })
    done();
});

gulp.task('default', gulp.series('browser-sync-init', 'nodemon', () => {
    gulp.watch(`${publicDir}/**/*.*`)
        .on('change', (path) => browserSync.reload(path))
        .on('add', browserSync.reload)
}))