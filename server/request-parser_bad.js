const qs = require('querystring');

module.exports = (req, res, next) => {
    requestData(req).then(next);
}

function requestData(request) {
    return new Promise((resolve, reject) => {
        let requestData = '';
        request.on('data', data => {
            requestData += data.toString();
        });
        request.on('end', () => {
            request.body = qs.parse(requestData);
            resolve();
        });
    });
}