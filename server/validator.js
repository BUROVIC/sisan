module.exports = function (schema = {}) {    
    return (req, res, next) => {
        let acceptObj = {};
        let invalidProps = [];
        Object.keys(schema).forEach(key => {
            if (req.body[key] && schema[key].test(req.body[key])) {
                acceptObj[key] = req.body[key];
            } else {
                invalidProps.push(key);
            }
        })
        if (invalidProps.length === 0) {
            req.body = acceptObj;
            next();
        } else {
            res.json({invalidProps});
        }
    }
}

