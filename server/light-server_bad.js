const http = require('http');
const fs = require('fs');
const url = require('url');
const path = require('path');
const mime = require('mime');
const zlib = require('zlib');

const sendFile = Symbol('sendFile');
const sendJson = Symbol('sendJson');
const createStream = Symbol('createStream');
const publicDir = Symbol('publicDir');
const req = Symbol('req');
const res = Symbol('res');
const error404 = Symbol('error404');
const requestArray = Symbol('requestArray');
const callbackChain = Symbol('callbackChain');
const createMethods = Symbol('createMethods');

class LightServer {
    constructor() {
        this[requestArray] = [];
        this[createMethods]();
        this.server = http.createServer(async (request, response) => {
            let startStatic = true;
            this[req] = request;
            this[res] = response;
            response.json = this[sendJson];
            const pathname = url.parse(request.url).pathname;
            console.dir(this[requestArray]);

            for (let reqElem of this[requestArray]) {
                if (request.method === reqElem.method.toUpperCase() && pathname === reqElem.url) {
                    for (let callback of reqElem.middleware) {
                        if (response.nonext) return;
                        if (!response.finished) await this[callbackChain](callback);
                    }
                    startStatic = false;
                }
            }

            if (!startStatic) return;

            if (this[publicDir] && request.method === 'GET') {
                this[sendFile](request, response);
            } else {
                this[error404](pathname);
            }
        })
    }

    // Public methods
    setRequest(method, url, ...middleware) {
        this[requestArray].push({ method, url, middleware });
    }

    listen(port) {
        this.server.listen(port);
    }

    staticServer(dirName) {
        this[publicDir] = dirName;
    }

    // Private methods
    [error404](pathname, response = this[res]) {
        response.writeHead(404);
        console.log('Resourse missing: 404 :' + pathname);
        response.write('Resourse missing: 404');
        response.end();
    }

    [createMethods]() {
        const HTTPMETHODS = ['get', 'post', 'put', 'patch', 'delete'];
        HTTPMETHODS.forEach(method => {
            this[method] = (url, ...middleware) => {
                this[requestArray].push({ method, url, middleware });
            }
        });
    }

    [callbackChain](callback, request = this[req], response = this[res]) {
        return new Promise((resolve, reject) => {
            const next = () => resolve();
            callback(request, response, next);
            setTimeout(() => {
                response.nonext = true;
                reject();
            }, 5000);
        }).then(() => {
            console.log("Next");
        }).catch(err => {
            if (err) console.error(err);
            else console.log("Can't find next expression");
        })
    }

    [sendJson](json, response = this[res] || this) {
        response.setHeader('Content-Type', `application/json; charset=utf-8`);
        const jsonString = JSON.stringify(json, null, 2);
        // console.log(jsonString);
        response.statusCode = 200;
        response.end(jsonString);
    }

    [sendFile](request = this[req], response = this[res]) {
        const parsedUrl = url.parse(request.url);
        const pathname = path.join(this[publicDir], parsedUrl.pathname);
        fs.stat(pathname, (error, stats) => {
            if (error) {
                this[error404](pathname);
            } else if (stats.isFile()) {
                this[createStream](pathname, response);
            } else if (!request.headers['x-requested-with']) {
                if (parsedUrl.pathname === '/') {
                    const indexPath = path.join(pathname, 'index.html');
                    this[createStream](indexPath, response);
                } else {
                    response.setHeader('Location', path.join(parsedUrl.pathname, 'index.html'));
                    response.statusCode = 301;
                    response.end();
                }
            }
        })
    }

    [createStream](pathname, response = this[res]) {
        const file = fs.createReadStream(pathname);
        file.on('open', () => {
            const gzip = zlib.createGzip();
            const mimeType = mime.getType(pathname);
            response.setHeader('Content-Type', mimeType + ';charset=utf-8');
            response.setHeader('Content-Encoding', 'gzip');
            response.statusCode = 200;
            file.pipe(gzip).pipe(response);
        })
        file.on("error", (err) => {
            console.log(err);
            response.statusCode = 403;
            response.write('file permission');
            response.end();
        });
    }
}

module.exports = () => new LightServer();