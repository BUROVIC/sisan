const qs = require('querystring');
const url = require('url');

module.exports = (req, res, next) => {
  requestData(req)
    .then(body => {
      req.body = body;
      next();
    })
    .catch(err => {
      next(err);
    })
}

function requestData(request) {
  let requestData = '';
  let body = url.parse(request.url, true).query;
  if (request.method === 'GET') return Promise.resolve(body);
  return new Promise((resolve, reject) => {
    request.on('data', (data) => {
      requestData += data.toString();
    });
    request.on('end', () => {
      try {
        if (request.headers["content-type"] === "application/x-www-form-urlencoded") {
          Object.assign(body, qs.parse(requestData));
          resolve(body);
        } else if (request.headers["content-type"] === "application/json") {
          Object.assign(body, JSON.parse(requestData));
          resolve(body);
        } else {
          throw new Error('Cannot read the data from user response');
        }
      } catch (err) {
        err.statusCode = 400;
        reject(err);
      }      
    });
    request.on('error', (err) => {
      reject(err);
    });
  })
}