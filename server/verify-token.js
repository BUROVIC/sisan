const jwt = require('jsonwebtoken');
const config = require('../config');
const mongodb = require('mongodb');

function verifyToken(req, res, next) {
    let bearerToken = req.headers['authorization'];
    if (!bearerToken) {
        res.statusCode = 403;
        res.json({ auth: false, message: 'No token provided.'});
        return;
    }

    let token = bearerToken.split(' ')[1];
    jwt.verify(token, config.secret, function (err, decodedToken) {
        if (err) {
            res.statusCode = 500;
            res.json({ auth: false, message: 'Failed to authenticate token.' });
            return;
        }
        req.userId = mongodb.ObjectId(decodedToken.id);
        next();
    });
}

module.exports = verifyToken;