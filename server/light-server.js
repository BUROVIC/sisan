const http = require('http');
const fs = require('fs');
const url = require('url');
const path = require('path');
const mime = require('mime');
const zlib = require('zlib');

// Set private schema names
const sendJson = Symbol('sendJson');
const sendData = Symbol('sendData');
const request = Symbol('request');
const response = Symbol('response');
const publicDir = Symbol('publicDir');
const sendFile = Symbol('sendFile');
const createStream = Symbol('createStream');
const requestArray = Symbol('requestArray');
const error404 = Symbol('error404');
const callbackChain = Symbol('callbackChain');
const nextTime = Symbol('nextTime');
const initHttpMethods = Symbol('initHttpMethods');

const HTTP_METHODS = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']

class LightServer {  
  constructor() {
    this[requestArray] = [];
    this[nextTime] = 20000;
    this[initHttpMethods]();
    
    this.server = http.createServer(async (req, res) => {
      let startStatic = true;
      res.json = this[sendJson];
      res.send = this[sendData];
      const pathname = url.parse(req.url).pathname.toLowerCase();
      Object.assign(this, { [request]: req, [response]: res });
      let isNext = true;
      let err;

      for (let reqElem of this[requestArray]) {
        if ((reqElem.method.toUpperCase() === req.method || reqElem.method === 'all') && 
            (reqElem.url.toLowerCase() === pathname || reqElem.url === 'all')) {
          for (let callback of reqElem.middleware) {
            if (isNext) {
              if (err && callback.length < 4) continue;
              if (!err && callback.length > 3) continue;
            } else return;
            if (!res.finished) [err, isNext] = await this[callbackChain](callback, err);
            else console.log('Your response is finished');
          }
          if (reqElem.url !== 'all' && reqElem.url !== '/') startStatic = false;
        }
      };

      if (err) {
        console.log("Cannot find next error hundler");
        console.error(err);
      }

      if (!startStatic) return;

      if (this[publicDir]) {
        this[sendFile](req, res);
      } else {
        this[error404](pathname);
      }
    });
    console.dir(this)
  }

  // Public methods

  listen(port) {
    this.server.listen(port);
  }

  staticServer(dirName) {
    this[publicDir] = dirName;
  }

  setRequest(method, url, ...middleware) {
    this[requestArray].push({ method, url, middleware });
  }

  use(...args) {
    const url = (typeof args[0] === 'string') ? args[0] : 'all';
    const middleware = args.filter(arg => typeof arg === 'function')
    this[requestArray].push({ method: 'all', url, middleware });
  }

  set nextWaitingTime(time) {
    if (typeof time == 'number' && time > 0) this[nextTime] = Math.round(time);
  }

  // Private methods

  [initHttpMethods]() {
    HTTP_METHODS.forEach(method => {
      method = method.toLowerCase();
      this.constructor.prototype[method] = function (url, ...middleware) {
        this[requestArray].push({ method, url, middleware });
      }
    })
  }

  [sendJson](json, res = this[response] || this) {
    res.setHeader('Content-Type', `application/json; charset=utf-8`);
    const jsonString = JSON.stringify(json, null, 2);
    res.statusCode = 200;
    res.end(jsonString);
  }

  [sendData](dataString, res = this[response] || this) {
    res.write(dataString);
  }

  [callbackChain](callback, error, req = this[request], res = this[response]){
    let isNext = false;
    return new Promise((resolve, reject) => {
      const next = (err) => {
        isNext = true;
        if (err instanceof Error) {
          reject(err);
        }
        resolve(isNext)
      };
      if (error) callback(error, req, res, next);
      else callback(req, res, next);

      setTimeout(() => {
        isNext = false;
        reject();
      }, this[nextTime]);
    }).then((isNext) => {
      console.log("Next middleware");
      return [null, isNext];
    }).catch((err = null) => {
      if (err && !isNext) console.error(err);
      else if (!err) console.log("Next call doesn't match");
      return [(isNext ? err : null), isNext];
    })
  }

  [error404](pathname, res = this[response]) {
    res.writeHead(404);
    console.log('Resourse missing: 404 :' + pathname);
    res.end('Resourse missing: 404');
  }

  [sendFile](req = this[request], res = this[response]) {
    const parsedUrl = url.parse(req.url);
    const pathname = path.join(this[publicDir], parsedUrl.pathname);
    fs.stat(pathname, (error, stats) => {
      if (error) {
        this[error404](pathname);
      } else if (stats.isFile()) {
        this[createStream](pathname, res);
      } else if (!req.headers['x-requested-with']) {
        if (parsedUrl.pathname === '/') {
          const indexPath = path.join(pathname, 'index.html');
          this[createStream](indexPath, res);
        } else {
          res.setHeader('Location', path.join(parsedUrl.pathname, 'index.html'));
          res.statusCode = 301;
          res.end();
        }
      }
    })
  }

  [createStream](pathname, res = this[response]) {
    const file = fs.createReadStream(pathname);
    file.on('open', () => {
      const gzip = zlib.createGzip();
      const mimeType = mime.getType(pathname);
      res.setHeader('Content-Type', mimeType + ';charset=utf-8');
      res.setHeader('Content-Encoding', 'gzip');
      res.statusCode = 200;
      file.pipe(gzip).pipe(res);
    })
    file.on("error", (err) => {
      console.log(err);
      res.statusCode = 403;
      res.write('file permission');
      res.end();
    });
  }
}

module.exports = () => new LightServer();