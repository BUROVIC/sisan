const path = require('path');

const app = require('./server/light-server')();
const requestParser = require('./server/request-parser');
const validator = require('./server/validator');
const usersController = require('./controllers/Users');
const verifyToken = require('./server/verify-token');

const tests = require('./controllers/Tests');
const statistics = require('./controllers/Statistics'); 

const loginValidator = validator({
    password: /^.{5,}$/,
    email: /^\w+\@\w+\.[a-z]{2,3}$/i
})

const signupValidator = validator({
    email: /^\w+\@\w+\.[a-z]{2,3}$/i,
    password: /^.{5,}$/
})

const tasks = require('./db/tasks.json');

const publicDir = path.join(__dirname, 'public');

const port = process.env.PORT || 4000;

app.staticServer(publicDir);

app.post('/api/signup', requestParser, signupValidator, usersController.createUser)
app.post('/api/login', requestParser, loginValidator, usersController.login)
app.get('/api/allusers', verifyToken, usersController.allUsers)
app.post('/api/verifyAnswer', requestParser, verifyToken, tests.verify);
app.get('/api/winrate', requestParser, verifyToken, statistics.currentUserWinRate);

app.listen(port);
console.log('Server started on http://localhost:' + port);


function saveUser(user) {
    return mongodb.connect("mongodb://localhost")
        .then(db => {
            const users = db.db('regexp').collection('users');
            console.log('Succesfull connection to Mongo');
            return users.insertOne(user);
        })
        .then(user => {
            console.log(user.ops);
            return user.ops;
        })
        .catch(error => console.error(error))    
}

function login(requestUser) {
    return mongodb.connect("mongodb://localhost")
        .then(db => {
            const users = db.db('regexp').collection('users');
            console.log('Succesfull connection to Mongo');
            return users.findOne({email: requestUser.email})
        })
        .then(userDB => {
            requestUser.password = requestUser.password || '';
            let compare = bcrypt.compareSync(requestUser.password, userDB.passwordHash);
            console.log(userDB);
            return (compare) ? userDB : {message: 'Wrong password'}
        })
        .catch(error => {message: 'Wrong password'})    
}